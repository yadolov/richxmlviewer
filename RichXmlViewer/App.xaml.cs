﻿using System.Windows;
using CefSharp;
using RichXmlViewer.Model;

namespace RichXmlViewer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var settings = new Settings();
            if (CEF.Initialize(settings))
            {
                CEF.RegisterScheme("proto", new SchemeHandlerFactory());
            }
        }

        protected override void OnExit(ExitEventArgs e)
        {
            CEF.Shutdown();

            base.OnExit(e);
        }
    }
}