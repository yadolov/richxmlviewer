﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using CefSharp;
using RichXmlViewer.Properties;

namespace RichXmlViewer.Model
{
    class SchemeHandler : ISchemeHandler
    {
        private readonly IDictionary<string, string> _textResources;
        private readonly IDictionary<string, Bitmap> _imgResources;

        public SchemeHandler()
        {
            _textResources = new Dictionary<string, string>
            {
                { "SelectFile.html", Resources.SelectFile },
                { "InvalidFile.html", Resources.InvalidFile },
            };
            _imgResources = new Dictionary<string, Bitmap>
            {
                { "warn.gif", Resources.warn },
            };
        }

        public bool ProcessRequestAsync(IRequest request, SchemeHandlerResponse response, OnRequestCompletedHandler requestCompletedCallback)
        {
            var uri = new Uri(request.Url);
            var segments = uri.Segments;
            var file = segments[segments.Length - 1];

            var fileExtension = Path.GetExtension(file);
            switch (fileExtension)
            {
                case ".html":
                {
                    string resource;
                    if (_textResources.TryGetValue(file, out resource) &&
                        !String.IsNullOrEmpty(resource))
                    {
                        var bytes = Encoding.UTF8.GetBytes(resource);
                        response.ResponseStream = new MemoryStream(bytes);
                        response.MimeType = "text/html";
                        requestCompletedCallback();

                        return true;
                    }
                    break;
                }
                case ".gif":
                {
                    Bitmap resource;
                    if (_imgResources.TryGetValue(file, out resource))
                    {
                        var memoryStream = new MemoryStream();
                        resource.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Gif);
                        response.ResponseStream = memoryStream;
                        response.MimeType = "image/gif";
                        requestCompletedCallback();

                        return true;
                    }
                    break;
                }
            }

            return false;
        }
    }
}