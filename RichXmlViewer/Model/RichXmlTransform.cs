﻿using System.IO;
using System.Xml;
using System.Xml.Xsl;
using RichXmlViewer.Properties;

namespace RichXmlViewer.Model
{
    static class RichXmlTransform
    {
        public static string Transform(string xmlFilePath)
        {
            var xslTransform = new XslCompiledTransform();
            using (var transformTextReader = new StringReader(Resources.transform))
            using (var transformXmlReader = new XmlTextReader(transformTextReader))
            {
                xslTransform.Load(transformXmlReader);
            }

            using (var outputStream = new MemoryStream())
            {
                xslTransform.Transform(xmlFilePath, null, outputStream);
                outputStream.Flush();
                outputStream.Position = 0;
                using (var sr = new StreamReader(outputStream))
                {
                    return sr.ReadToEnd();
                }
            }
        }
    }
}