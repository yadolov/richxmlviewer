﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using RichXmlViewer.Properties;

namespace RichXmlViewer.Model
{
    class RichXmlValidator
    {
        private readonly List<string> _errors = new List<string>();

        public List<string> Errors
        {
            get { return _errors; }
        }

        public bool Validate(string xmlFilePath)
        {
            _errors.Clear();

            var readerSettings = new XmlReaderSettings();
            using (var schemaTextReader = new StringReader(Resources.schema))
            using (var schemaXmlReader = new XmlTextReader(schemaTextReader))
            {
                readerSettings.Schemas.Add("http:/consultant.ru/nris/test", schemaXmlReader);
                readerSettings.ValidationType = ValidationType.Schema;
                readerSettings.ValidationEventHandler += ValidationHandler;

                using (var xmlReader = XmlReader.Create(xmlFilePath, readerSettings))
                {
                    while (xmlReader.Read())
                    {
                    }
                }
            }

            return _errors.Count == 0;
        }

        private void ValidationHandler(object sender, ValidationEventArgs args)
        {
            _errors.Add(args.Message);
        }
    }
}