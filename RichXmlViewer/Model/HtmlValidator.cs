﻿using System.IO;
using System.Linq;
using System.Text;
using HtmlAgilityPack;

namespace RichXmlViewer.Model
{
    static class HtmlValidator
    {
        private const string TagNotClosedText = "не закрыт";
        private const string TagNotOpenedText = "не открыт";
        private const string ImageWithErrorFormat =
            "<img src=\"proto://site/warn.gif\" height=\"24\" width=\"24\" title=\"Тег {0} {1} (Строка: {2} Столбец: {3})\">";

        public static string Validate(string html)
        {
            using (var stringReader = new StringReader(html))
            {
                var htmlDocument = new HtmlDocument
                {
                    OptionAutoCloseOnEnd = true
                };
                htmlDocument.Load(stringReader);
                var errors = htmlDocument.ParseErrors.Where(
                    e => e.Code == HtmlParseErrorCode.TagNotOpened ||
                         e.Code == HtmlParseErrorCode.TagNotClosed)
                    .OrderByDescending(e => e.StreamPosition);
                var sb = new StringBuilder(html);
                foreach (var error in errors)
                {
                    string reason;
                    switch (error.Code)
                    {
                        case HtmlParseErrorCode.TagNotClosed:
                            reason = TagNotClosedText;
                            break;
                        case HtmlParseErrorCode.TagNotOpened:
                            reason = TagNotOpenedText;
                            break;
                        default:
                            continue;
                    }
                    var imageWithErrorText =
                        string.Format(ImageWithErrorFormat, error.SourceText, reason, error.Line, error.LinePosition);
                    sb.Insert(error.StreamPosition, imageWithErrorText);
                }
                return sb.ToString();
            }
        }
    }
}