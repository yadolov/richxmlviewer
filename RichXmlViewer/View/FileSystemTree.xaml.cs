﻿using System.Windows;
using System.Windows.Controls;
using RichXmlViewer.ViewModel;

namespace RichXmlViewer.View
{
    /// <summary>
    /// Interaction logic for FileSystemTree.xaml
    /// </summary>
    public partial class FileSystemTree : UserControl
    {
        private MainViewModel _myViewModel; 

        public FileSystemTree()
        {
            InitializeComponent();
            Loaded += ViewLoaded;
        } 

        private void ViewLoaded(object sender, RoutedEventArgs r)
        {
            _myViewModel = DataContext as MainViewModel;
            (DirectoryTree.Items[0] as DirInfo).IsSelected = true;
        }

        private void DirectoryTree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            _myViewModel.FileTreeVM.CurrentTreeItem = DirectoryTree.SelectedItem as DirInfo;
        }

        private void TreeView_Expanded(object sender, RoutedEventArgs e)
        {
            var currentTreeNode = sender as TreeViewItem;
            if (currentTreeNode == null)
                return;

            if (currentTreeNode.ItemsSource == null)
                return;

            var parentDirectory = currentTreeNode.Header as DirInfo;
            if (parentDirectory == null)
                return;

            foreach (DirInfo d in currentTreeNode.ItemsSource)
            {
                if (_myViewModel.CurrentDirectory.Path.Equals(d.Path))
                {
                    d.IsSelected = true;
                    d.IsExpanded = true;
                    break;
                }
            }
            e.Handled = true;
        } 
    }
}