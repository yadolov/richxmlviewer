﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using RichXmlViewer.ViewModel;

namespace RichXmlViewer.View
{
    /// <summary>
    /// Interaction logic for DirectoryViewer.xaml
    /// </summary>
    public partial class DirectoryViewer : UserControl
    {
        private MainViewModel _viewModel; 

        public DirectoryViewer()
        {
            InitializeComponent();
            Loaded += DirectoryViewer_Loaded;
        } 

        void DirectoryViewer_Loaded(object sender, RoutedEventArgs e)
        {
            _viewModel = DataContext as MainViewModel;
        }

        private void dirList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            _viewModel.DirViewVM.OpenCurrentObject();
        }

        private void dirList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                _viewModel.DirViewVM.OpenCurrentObject();
            }
        } 
    }
}
