﻿using System.Windows;
using RichXmlViewer.ViewModel;

namespace RichXmlViewer.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            (DataContext as MainViewModel).WebBrowser = WebView;
        }
    }
}