<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:my="http:/consultant.ru/nris/test"
    exclude-result-prefixes="my">

    <xsl:output method="html" indent="yes" encoding="utf-8"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="/">
    <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html></xsl:text>
    <html>
    <head>
    <title>task</title>
    </head>
    <body>
    <xsl:apply-templates/>
    </body>
    </html>
    </xsl:template>

    <xsl:template match="my:Data">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="my:Group1 | my:Group2">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="my:Entity">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="my:RichText">
        <xsl:value-of select="text()" disable-output-escaping="yes"/>
    </xsl:template>

    <xsl:template match="*"/>
</xsl:stylesheet>
