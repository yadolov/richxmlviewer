﻿using GalaSoft.MvvmLight;

namespace RichXmlViewer.ViewModel
{
    public class DirectoryViewerViewModel : ViewModelBase
    {
        private readonly MainViewModel _evm;

        public DirectoryViewerViewModel(MainViewModel evm)
        {
            _evm = evm;
        } 

        public DirInfo CurrentItem { get; set; }

        public void OpenCurrentObject()
        {
            if ((ObjectType)CurrentItem.DirType == ObjectType.File)
            {
                _evm.OpenFileContent(CurrentItem.Path);
            }
            else
            {
                _evm.CurrentDirectory = CurrentItem;
                _evm.FileTreeVM.ExpandToCurrentNode(_evm.CurrentDirectory);
            }
        } 
    }
}