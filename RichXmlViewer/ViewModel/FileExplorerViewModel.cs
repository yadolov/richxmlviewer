﻿using System.Collections.Generic;
using GalaSoft.MvvmLight;
using RichXmlViewer.Properties;

namespace RichXmlViewer.ViewModel
{
    public class FileExplorerViewModel : ViewModelBase
    {
        private readonly MainViewModel _evm;
        private DirInfo _currentTreeItem;
        private IList<DirInfo> _sysDirSource; 

        public IList<DirInfo> SystemDirectorySource
        {
            get { return _sysDirSource; }
            set
            {
                _sysDirSource = value;
                RaisePropertyChanged(() => SystemDirectorySource);
            }
        }

        public DirInfo CurrentTreeItem
        {
            get { return _currentTreeItem; }
            set
            {
                _currentTreeItem = value;
                _evm.CurrentDirectory = _currentTreeItem;
            }
        } 

        public FileExplorerViewModel(MainViewModel evm)
        {
            _evm = evm;

            var rootNode = new DirInfo(Resources.My_Computer_String)
            {
                Path = Resources.My_Computer_String
            };
            _evm.CurrentDirectory = rootNode;

            SystemDirectorySource = new List<DirInfo> { rootNode };
        } 

        public void ExpandToCurrentNode(DirInfo curDir)
        {
            if (CurrentTreeItem == null ||
                (!curDir.Path.Contains(CurrentTreeItem.Path) && CurrentTreeItem.Path != Resources.My_Computer_String))
                return;
            CurrentTreeItem.IsExpanded = false;
            CurrentTreeItem.IsExpanded = true;
        } 
    }
}