using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using CefSharp;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using RichXmlViewer.Model;
using RichXmlViewer.Properties;

namespace RichXmlViewer.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private const string FileSearchMask = "*.xml";
        private DirInfo _currentDirectory;
        private FileExplorerViewModel _fileTreeVM;
        private DirectoryViewerViewModel _dirViewerVM;
        private IList<DirInfo> _currentItems;
        private bool _showDirectoryTree = true;
        private ICommand _showTreeCommand;

        public MainViewModel()
        {
            FileTreeVM = new FileExplorerViewModel(this);
            DirViewVM = new DirectoryViewerViewModel(this);
            ShowTreeCommand = new RelayCommand(DirectoryTreeHideHandler);
        }

        public DirInfo CurrentDirectory
        {
            get { return _currentDirectory; }
            set
            {
                _currentDirectory = value;
                RefreshCurrentItems();
                RaisePropertyChanged(() => CurrentDirectory);
            }
        }

        public FileExplorerViewModel FileTreeVM
        {
            get { return _fileTreeVM; }
            set
            {
                _fileTreeVM = value;
                RaisePropertyChanged(() => FileTreeVM);
            }
        }

        public bool ShowDirectoryTree
        {
            get { return _showDirectoryTree; }
            set
            {
                _showDirectoryTree = value;
                RaisePropertyChanged(() => ShowDirectoryTree);
            }
        }

        public ICommand ShowTreeCommand
        {
            get { return _showTreeCommand; }
            set
            {
                _showTreeCommand = value;
                RaisePropertyChanged(() => ShowTreeCommand);
            }
        }

        public DirectoryViewerViewModel DirViewVM
        {
            get { return _dirViewerVM; }
            set
            {
                _dirViewerVM = value;
                RaisePropertyChanged(() => DirViewVM);
            }
        }

        public IList<DirInfo> CurrentItems
        {
            get { return _currentItems ?? (_currentItems = new List<DirInfo>()); }
            set
            {
                _currentItems = value;
                RaisePropertyChanged(() => CurrentItems);
            }
        }

        public IWebBrowser WebBrowser { get; set; }

        public void OpenFileContent(string filePath)
        {
            var xmlValidator = new RichXmlValidator();
            if (xmlValidator.Validate(filePath))
            {
                var html = RichXmlTransform.Transform(filePath);
                var validatedHtml = HtmlValidator.Validate(html);
                WebBrowser.LoadHtml(validatedHtml);
                //WebBrowser.Load(string.Format("file://{0}", filePath));
            }
            else
            {
                WebBrowser.Load("proto://site/InvalidFile.html");
            }
        }

        private void DirectoryTreeHideHandler()
        {
            ShowDirectoryTree = false;
        }

        protected void RefreshCurrentItems()
        {
            IList<DirInfo> childDirList;

            if (CurrentDirectory.Name.Equals(Resources.My_Computer_String))
            {
                childDirList = (from rd in FileSystemExplorerService.GetRootDirectories()
                                select new DirInfo(rd)).ToList();
            }
            else
            {
                childDirList =
                    (from dir in FileSystemExplorerService.GetChildDirectories(CurrentDirectory.Path)
                        select new DirInfo(dir)).ToList();

                IList<DirInfo> childFileList =
                    (from fobj in FileSystemExplorerService.GetChildFiles(CurrentDirectory.Path, FileSearchMask)
                        select new DirInfo(fobj)).ToList();

                childDirList = childDirList.Concat(childFileList).ToList();
            }

            CurrentItems = childDirList;
        } 
    }
}