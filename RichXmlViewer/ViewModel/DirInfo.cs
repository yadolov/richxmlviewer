﻿using System.Collections.Generic;
using System.IO;
using System.Windows;

namespace RichXmlViewer.ViewModel
{
    public enum ObjectType
    {
        MyComputer = 0,
        DiskDrive = 1,
        Directory = 2,
        File = 3
    }

    public class DirInfo : DependencyObject
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string Root { get; set; }
        public string Size { get; set; }
        public string Ext { get; set; }
        public int DirType { get; set; }

        public static readonly DependencyProperty propertyChilds = DependencyProperty.Register("Childs", typeof(IList<DirInfo>), typeof(DirInfo));
        public IList<DirInfo> SubDirectories
        {
            get { return (IList<DirInfo>)GetValue(propertyChilds); }
            set { SetValue(propertyChilds, value); }
        }

        public static readonly DependencyProperty propertyIsExpanded = DependencyProperty.Register("IsExpanded", typeof(bool), typeof(DirInfo));
        public bool IsExpanded
        {
            get { return (bool)GetValue(propertyIsExpanded); }
            set { SetValue(propertyIsExpanded, value); }
        }

        public static readonly DependencyProperty propertyIsSelected = DependencyProperty.Register("IsSelected", typeof(bool), typeof(DirInfo));
        public bool IsSelected
        {
            get { return (bool)GetValue(propertyIsSelected); }
            set { SetValue(propertyIsSelected, value); }
        } 

        public DirInfo()
        {
            SubDirectories = new List<DirInfo> { new DirInfo("TempDir") };
        }

        public DirInfo(string directoryName)
        {
            Name = directoryName;
        }

        public DirInfo(DirectoryInfo dir)
            : this()
        {
            Name = dir.Name;
            Root = dir.Root.Name;
            Path = dir.FullName;
            DirType = (int)ObjectType.Directory;
        }

        public DirInfo(FileInfo fileobj)
        {
            Name = fileobj.Name;
            Path = fileobj.FullName;
            DirType = (int)ObjectType.File;
            Size = (fileobj.Length / 1024) + " KB";
            Ext = fileobj.Extension + " File";
        }

        public DirInfo(DriveInfo driveobj)
            : this()
        {
            Name = driveobj.Name.EndsWith(@"\") ?
                driveobj.Name.Substring(0, driveobj.Name.Length - 1) : driveobj.Name;

            Path = driveobj.Name;
            DirType = (int)ObjectType.DiskDrive;
        } 
    }
}